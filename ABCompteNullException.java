package tp;

public class ABCompteNullException extends Exception {
	private static final long serialVersionUID = 1L;
	private String message ;
	
	public ABCompteNullException(String string) {
		this.message = string;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
