package tp;

import java.util.ArrayList;

public class Agence {
	
	private String nom_agence ;
	private String location_agence ;
	static ArrayList<compte> liste_Compte = new ArrayList<compte>();
	
	public Agence() {}
	public Agence(String nom_agence, String location_agence) {
		this.nom_agence = nom_agence;
		this.location_agence = location_agence;
	}
	public String getNom_agence() {
		return nom_agence;
	}
	public void setNom_agence(String nom_agence) {
		this.nom_agence = nom_agence;
	}
	public String getLocation_agence() {
		return location_agence;
	}
	public void setLocation_agence(String location_agence) {
		this.location_agence = location_agence;
	}
	public int getnbcomptes()
	{
		return liste_Compte.size();
	}
	
	private int trouveCompte(String pNC)
	{
		for (int i = 0; i < liste_Compte.size(); i++) {
			if(liste_Compte.get(i).getNum_compte().equals(pNC))
			{
				return i ;
			}
		}
		return -1 ;
		
	}
	static int trouver(String pNC)
	{
		Agence a = new Agence();
		return a.trouveCompte(pNC);
	}
	public void addCompte(compte pCompteAAjouter) throws ABCompteDejaExistantException , ABCompteNullException
	{
		for (int i = 0; i < liste_Compte.size(); i++) {
			if (liste_Compte.get(i).getNum_compte() == pCompteAAjouter.getNum_compte()) {
				throw new ABCompteDejaExistantException("compte deja existe");
			}
		}
		if (pCompteAAjouter.getNom_prop() == null && pCompteAAjouter.getNum_compte() == null)
		{
			throw new ABCompteNullException("le compte que vous entrez est incomplet");
		}	
		
		
	}
}
