package tp;

public class compte {
	
	private String num_compte ;
	private String nom_prop ;
	
	public compte(String num_compte, String nom_prop) {
		super();
		this.num_compte = num_compte;
		this.nom_prop = nom_prop;
	}

	public String getNum_compte() {
		return num_compte;
	}

	public void setNum_compte(String num_compte) {
		this.num_compte = num_compte;
	}

	public String getNom_prop() {
		return nom_prop;
	}

	public void setNom_prop(String nom_prop) {
		this.nom_prop = nom_prop;
	}
	
}
